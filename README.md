# SB_300

### What's about?
NamedParameterJdbcTemplate connects onto H2 database 

* on @EventListener (at ContextRefreshedEvent) initialize h2 table+data, and
* on /login login.html form POST request submit search the same h2 / username+pw 


### 1. pom.xml
* web, H2, jdbc API, thymeleaf, and ...
* spring-boot-configuration-processor

### 2. application.yml
First write the default rowy in the app.properties -> copy the file as app.yml -> delete the app.props file.

* server.port: 55555
* spring.h2.console.enabled: true, spring.h2.console.path: "/db"
* thymeleaf:cache: true
* app:db:   jdbc-url: "jdbc:h2:mem:testdb;DB_CLOSE_DELAY= -1" username: "sa"  password: "" driver-class-name: "org.h2.Driver"

### 3. H2 table init
* @Configuration class with @Bean @ConfigurationProperties("app.db") DataSource..build @Bean new NamedParameterJdbcTemplate(dataSource)
* dao. @Autowired NamedParameterJdbcTemplate jdbc.execute SQLs: DROP TABLE IF EXISTS, CREATE TABLE, INSERT INTO login_user ..
* system. @Component class @EventListener method (ContextRefreshedEvent) calls jdbc.execute

### 4. HTML form submitted data into model

### 5. @Controller on form POST, compares with H2

### What's about?