package siample.dev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import siample.dev.domain.Login_user;
import siample.dev.form.LoginForm;
import siample.dev.service.LoginService;

@Controller
public class LoginController {

	@Autowired LoginService loginService;
	
	/**
	 * . . . /login -> http GET hívással 
	 * @return login.html
	 */
	@RequestMapping(value = "/login", method=RequestMethod.GET )
	public String getLoginFormPage() {
		return "login";
	}
	

/**
 * . . . /login -> http POST submit történt, és a submitted értékek  
 * @param loginForm objektumba kerültek, és a
 * @param model     kiki nevű obj.ába is kerültek. 
 * @return
 */
	@RequestMapping(value = "/login", method=RequestMethod.POST )
	public String doLogin(@ModelAttribute (name="kiki") LoginForm loginForm, Model model) {
			
		String username = loginForm.getUsern();
		String password = ((LoginForm) model.getAttribute("kiki")).getPassw();
		System.out.println("typed usersname: "+username+"\ttyped password:" + password);

		Login_user loginUserFromDb = null;
		
		loginUserFromDb = loginService.findLoginUserByUnamePw(username, password);

		if (loginUserFromDb != null) {
			return "home";
		}
       
/* 1.   if ("a".equals(username) && "a".equals(password) ) {
			return "home";
		} 
*/			
		
/*2.	Csúnya, mert tábla összes row-ját utaztatja a java:
 		List<Login_user> db_login_users = loginService.findLoginUsers(); 
		
		for (Login_user db_login_user : db_login_users) {
			if ( username.equals(db_login_user.getUsername()) && password.equals(db_login_user.getPassword()) ) {
				return "home";
			}
		}
*/
		model.addAttribute("invalidCredentials", true);
		return "login";
	} 
}
 