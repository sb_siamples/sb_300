package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb300Application {

	public static void main(String[] args) {
		SpringApplication.run(Sb300Application.class, args);
	}

}
