package siample.dev.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import siample.dev.domain.Login_user;
import siample.dev.repository.LoginRepository;

@Service
public class LoginService {

	@Autowired LoginRepository loginRepository;
	
	/**
	 * 1.
	 */
	public List<Login_user> findLoginUsers() {
		//return loginRepository.findAll();
		return loginRepository.findAll2();
	}
	
	/**
	 * 2.
	 */
//	public Login_user findLoginUserByUnamePw(String username, String password) { // public List<Login_user> findLoginUsers() {
		//return loginRepository.findByUsernamePassword(username, password);// return loginRepository.findAll();
	public Login_user findLoginUserByUnamePw(String username, String password) {
		return loginRepository.findByUsernamePassword(username, password);
	}
}
