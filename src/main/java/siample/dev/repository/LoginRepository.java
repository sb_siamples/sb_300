package siample.dev.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import siample.dev.domain.Login_user;

@Repository
public class LoginRepository {

	@Autowired
	NamedParameterJdbcTemplate jdbc;

	/**
	 * Az összes felhasználó lekérdezése, csúnya: csak rs resultset
	 * 
	 * @return
	 */
	public List<Login_user> findAll() {
		return jdbc.query("select * from login_user",
				(rs, rowNum) -> new Login_user(rs.getLong("id"), rs.getString("username"), rs.getString("password")));
	}

	/**
	 * Az összes felhasználó lekérdezése, szép: BeanPropertyRowMapper
	 */
	public List<Login_user> findAll2() {
		String sql = "SELECT * FROM login_user";
		return jdbc.query(sql, BeanPropertyRowMapper.newInstance(Login_user.class));
	}

	public Login_user findByUsernamePassword(String username, String password) {
		
		System.out.println(username + "__________________________" + password);

        /**
         * 3.a ha rossz login adatok begépelése esetén BeanPropertyRowMapper.newInstance(Login_user.class)); Exception-t dob
         */
		String sql = "select * from login_user where username=:unparam and password=:pwparam";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("unparam", username); // , Types.Date);
		namedParameters.addValue("pwparam", password); // , Types.Integer);
		Login_user result = null;
		
		try {
			result = jdbc.queryForObject(
					sql, 
					namedParameters, 
					new BeanPropertyRowMapper<>(Login_user.class));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = null;
		}
	             // new BeanPropertyRowMapper(Login_user.class));
		         // BeanPropertyRowMapper.newInstance(Login_user.class));		
		// nested exception is org.springframework.dao.EmptyResultDataAccessException: Incorrect result size: expected 1, actual 0] with root cause
		   		
		System.out.println("Repo return result Login_user objektum értéke: " + result.toString());
		
		return result;
	}
}
