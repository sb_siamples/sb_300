package siample.dev.form;
/**
 *
 */
public class LoginForm {
	
	private String usern; // html form űrlapon input type="text"     name="usern"
	private String passw; // html form űrlapon input type="password" name="passw"
	
	public LoginForm() {
		
	}

	public LoginForm(String username, String password) {
		this.usern = username;
		this.passw = password;
	}

	public String getUsern() {
		return usern;
	}

	public void setUsern(String usern) {
		this.usern = usern;
	}

	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}
	

}
